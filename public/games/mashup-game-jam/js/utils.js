class Utils{
    static approach(value, target, amount){
        if(value < target){
            value += amount;
            if(value > target){
                return target;
            }
        }else{
            value -= amount;
            if(value < target){
                return target;
            }
        }
        return value;
    }

    static showButtons(button){
        let x = 420;
        if(button == "z"){
            if(keyDown("z")){
                image(graphicsHandler.assets.sprites.buttons.z.down,x,50);
            }else{
                image(graphicsHandler.assets.sprites.buttons.z.up,x,50);
            }
        }
        if(button == "x"){
            if(keyDown("x")){
                image(graphicsHandler.assets.sprites.buttons.x.down,x,100);
            }else{
                image(graphicsHandler.assets.sprites.buttons.x.up,x,100);
            }
        }
        if(button == "c"){
            if(keyDown("c")){
                image(graphicsHandler.assets.sprites.buttons.c.down,x,150);
            }else{
                image(graphicsHandler.assets.sprites.buttons.c.up,x,150);
            }
        }
        
    }
}