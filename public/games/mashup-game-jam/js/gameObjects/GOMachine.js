class GOMachine extends GameObject{
    active = false;
    level = 0;
    barX = 138;
    barY = 5;
    //TODO: fix
    barWidth = 200;
    barHeight = 710;
    range = 0;
    levelReached = 0;
    levelMax = 3;

    constructor(x,y,sprite){
        super(x,y,sprite);
    }

    init(){

    }

    step(){
        //super.step();
        if(this.getPlayerDistance() <= this.range && this.levelReached < this.levelMax){
            if(!this.active){
                this.init();
            }
            this.active = true;
            
        }else{
            this.active = false;
        }
        this.sprite.depth = this.y + this.sprite.height/2;
        this.sprite.position.x = GraphicsHandler.WORLD_WIDTH/2;
        this.sprite.position.y = GraphicsHandler.WORLD_HEIGHT/2;
        
    }

    draw(){
        if(this.active){
            fill(255,0,0);
            //rect(this.barX,this.barY,this.barWidth,this.barHeight);
            image(graphicsHandler.assets.sprites.bar,GraphicsHandler.VIEWPORT_WIDTH/2,GraphicsHandler.VIEWPORT_HEIGHT/2);
        }
    }

    getPlayerDistance(){
        let player = room.gym.player;
        let distX = 0;
        let distY = 0;

        if(player != undefined){
            distX = this.x - player.x;
            distY = this.y - player.y;
            return(sqrt(distX*distX + distY*distY));
        }else{
            //console.error("Player not found by machine");
        }
    }

    playBarSfx(level){

        let audio;
        if(level == 1){
            audio = AudioHandler.getSfx("bar-0");
        }
        if(level == 2){
            audio = AudioHandler.getSfx("bar-1");
        }
        if(level == 3){
            audio = AudioHandler.getSfx("bar-2");
        }
        if(audio != undefined){
            audio.play();
        }
    }

    showButtons(button){
        let x = 420;
        if(button == "z"){
            if(keyDown("z")){
                image(graphicsHandler.assets.sprites.buttons.z.down,x,50);
            }else{
                image(graphicsHandler.assets.sprites.buttons.z.up,x,50);
            }
        }
        if(button == "x"){
            if(keyDown("x")){
                image(graphicsHandler.assets.sprites.buttons.x.down,x,100);
            }else{
                image(graphicsHandler.assets.sprites.buttons.x.up,x,100);
            }
        }
        if(button == "c"){
            if(keyDown("c")){
                image(graphicsHandler.assets.sprites.buttons.c.down,x,150);
            }else{
                image(graphicsHandler.assets.sprites.buttons.c.up,x,150);
            }
        }
        
    }

    forCutscene(){
        this.range = -1;
        this.y -= 1080;
    }
}