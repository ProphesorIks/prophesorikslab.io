class GOTreadmill extends GOMachine{
    x = 72;
    y = 850;
    range = 64;
    levelReached = playerStats.stamina;
    levelMax = 4;

    step(){
        super.step();
        if(this.active){
            if(keyWentDown(CONTROLS.LEFT) || keyWentDown(CONTROLS.RIGHT)){
                let modifier = 0.01;
                playerStats.stamina += modifier * (playerStats.chalk + 1);
                playerStats.chalk = Utils.approach(playerStats.chalk, 0, .05);
                playerStats.stamina= constrain(playerStats.stamina, 0,3);
                if(floor(playerStats.stamina) != this.levelReached){
                    this.levelReached = floor(playerStats.stamina);
                    playerStats.staminaBase = this.levelReached;
                    this.playBarSfx(this.levelReached);
                    graphicsHandler.screenShake(150,.9);
                }
                graphicsHandler.screenShake(5,.9);
                AudioHandler.playTredmill();
            }
        }
    }

    draw(){
        //super.draw();
        if(this.active){
            this.showButtons("x");
            this.showButtons("z");
            graphicsHandler.drawStaminaBar();
        //     let barHeight = (playerStats.body.arms * this.barHeight) /3; //4 cuz 4 levels of arms
        //     fill(0,200,0);
        //     rect(this.barX,this.barY + this.barHeight - barHeight,this.barWidth,barHeight);
        }
    }
}