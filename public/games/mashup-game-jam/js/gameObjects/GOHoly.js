class GOHoly extends GameObject{
    liftLevel = 0;
    liftLevelMax = 800;
    gravity = 1;
    gravityMax = 40;
    speed = 0; //how quickly dumbell falls
    canLift = true;
    heLifted = false;
    heWeak = false;
    staminaModifier = .05;


    constructor(x,y,sprite){
        super(x,y,sprite);
        console.log(this);
        //this.sprite.visible = true;
    }

    step(){
        playerStats.staminaBase = playerStats.staminaBaseLast;
        if(this.speed < this.gravityMax){
            this.speed += this.gravity;
        }
        this.liftLevel = Utils.approach(this.liftLevel, 0, this.speed);
        this.updateSprite();
        this.controlls();
        this.y = height - (this.liftLevel*height)/this.liftLevelMax,5;

        if(playerStats.stamina > 0 && !this.heLifted && !this.heWeak){
            camera.off();
            Utils.showButtons('z');
            Utils.showButtons('x');
            camera.on();
        }
            
        if(this.y <= 128 && !this.heLifted){
            this.canLift = false;
            graphicsHandler.screenShake(1000,0.995);
            this.heLifted = true;
            this.won();
        }
        if(playerStats.stamina <= 0 && !this.heWeak){
            this.heWeak = true;
            this.canLift = false;
            if(!this.heLifted){
                this.lost();
            }
        }
        if(this.heLifted || this.heWeak){
            if(keyWentUp('c')){
                //graphicsHandler.transition(-.02,0,() => {gameState = GAME_STATES.MENU});
            }
            //playerStats.stamina = 0;
            camera.off();
            //Utils.showButtons('c');
            camera.on();
        }
    }

    controlls(){
        if(this.canLift){
            if(keyWentUp(CONTROLS.RIGHT) || keyWentUp(CONTROLS.LEFT)){
                let modifier = 1.1;
                let struggle = this.liftLevel/this.liftLevelMax * .5;
                let liftPower = 2;
                liftPower += playerStats.body.arms;
                liftPower += playerStats.body.legs;
                liftPower += playerStats.body.body;
                liftPower += playerStats.body.head * 2;
                // if(liftPower >= 10){
                //     this.liftLevel = Utils.approach(this.liftLevel, 800, 100);
                // }
                liftPower *= (playerStats.chalk + 1)/2;
                liftPower *= modifier;
                //liftPower -= struggle;
                this.liftLevel += liftPower;
                graphicsHandler.screenShake(((this.liftLevel/this.liftLevelMax)*4 +1 ) * 15, .8);
                this.speed = 0;
                playerStats.stamina -=this.staminaModifier;
                AudioHandler.playHoly();
            }
        }
    }

    won(){
        AudioHandler.getSfx("lightning").play();
        graphicsHandler.transition(.05,0,() => {});
        AudioHandler.getMusic("win").play();
        setTimeout(function(){
            if(gameState == GAME_STATES.LIFT){
                AudioHandler.getSfx("end").play();
                setTimeout(function(){
                    if(gameState == GAME_STATES.LIFT){
                    graphicsHandler.transition(-.02,255,() => {gameState = GAME_STATES.MENU});
                    }
                },8000);
            }
        },1000*26);
    }

    lost(){
        AudioHandler.getSfx("lightning").play();
        graphicsHandler.transition(.05,0,() => {});
        AudioHandler.getMusic("lose").play();
        setTimeout(function(){
            graphicsHandler.transition(-.02,0,() => {gameState = GAME_STATES.MENU});
        },9000);
    }

}