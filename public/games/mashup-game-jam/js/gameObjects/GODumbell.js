class GODumbell extends GOMachine{
    x = 240;
    y = 30;
    range = 64;
    isUsed = false;
    step(){
        super.step();
        if(this.active){
            if(keyWentDown(CONTROLS.USE) && room.gym.timeOver && !this.isUsed){
                graphicsHandler.transition(-.01, 255, () => {gameState = GAME_STATES.LIFT});
                this.isUsed = true;
            }
        }
        this.sprite.depth = this.y;
    }

    draw(){
        if(this.active && room.gym.timeOver){
            this.showButtons("c");
        }
    }
}