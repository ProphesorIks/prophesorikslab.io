class GOChalk extends GOMachine{
    x = 433;
    y = 900;
    range = 64;
    levelReached = playerStats.chalk;

    step(){
        super.step();
        if(this.active){
            if(keyWentDown(CONTROLS.USE)){
                playerStats.chalk += .2;
                playerStats.chalk = constrain(playerStats.chalk, 0, 3);
                if(playerStats.chalk < 3){
                    AudioHandler.getSfx("chalk").play();
                }
            }
        }
        
        //     let modifier = 0.01;
        //     playerStats.body.arms += modifier * (playerStats.chalk + 1);
        //     playerStats.chalk = Utils.approach(playerStats.chalk, 0, .05);
        //     playerStats.body.arms = constrain(playerStats.body.arms, 0,3);
        //     if(floor(playerStats.body.arms) != this.levelReached){
        //         this.levelReached = floor(playerStats.body.arms);
        //         this.playBarSfx(this.levelReached);
                
        //     }
        //     graphicsHandler.screenShake(5,.9);
        // }
        // this.sprite.depth = this.y -60;
    }

    draw(){
        // super.draw();
         if(this.active){
            this.showButtons("c");
         }
        //     let barHeight = (playerStats.body.arms * this.barHeight) /3; //4 cuz 4 levels of arms
        //     fill(0,200,0);
        //     rect(this.barX,this.barY + this.barHeight - barHeight,this.barWidth,barHeight);
        // }
    }
}