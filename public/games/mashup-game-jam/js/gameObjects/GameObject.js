class GameObject {
    x;
    y;
    sprite;

    constructor(x, y, sprite) {
        this.x = x;
        this.y = y;
        this.sprite = sprite;
        if(this.sprite != undefined){
            this.sprite.visible = true;
        }
    }

    step(){ };

    updateSprite() {
        if(this.sprite !=undefined){
            this.sprite.position.x = this.x;
            this.sprite.position.y = this.y;
        }
    }

    //prevents object from goin offscreen
    constrainToWorld() {
        if(this.sprite != undefined){
            this.x = constrain(this.x, this.sprite.width / 2, GraphicsHandler.WORLD_WIDTH - this.sprite.width / 2);
            this.y = constrain(this.y, this.sprite.height / 2, GraphicsHandler.WORLD_HEIGHT - this.sprite.height / 2);
        }
    }

    //clean up
    delete(){
        if(this.sprite != undefined){
            this.sprite.visible = false;
        }
    }
}
