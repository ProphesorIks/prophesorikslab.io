class GODrugs extends GOMachine{
    x = 433;
    y = 260;
    range = 64;
    used = false;

    step(){
        super.step();
        if(this.active && !this.used){
            if(keyWentDown(CONTROLS.USE)){
                let rng = floor(random(7));
                console.log(rng);
                if(rng == 0){
                    playerStats.body.head = 3;
                }
                if(rng == 2){
                    playerStats.chalk = 3;
                }
                if(rng == 3){
                    playerStats.stamina = 1;
                    playerStats.staminaBase = 1;
                }
                if(rng == 4){
                    playerStats.body.legs = 0;
                }
                if(rng == 5){
                    playerStats.body.body = 3;
                }
                if(rng == 6){
                    playerStats.body.arms = 0;
                }
                this.used = true;
                AudioHandler.getSfx("drugs").play();
            }
            
        }
        
        //     let modifier = 0.01;
        //     playerStats.body.arms += modifier * (playerStats.chalk + 1);
        //     playerStats.chalk = Utils.approach(playerStats.chalk, 0, .05);
        //     playerStats.body.arms = constrain(playerStats.body.arms, 0,3);
        //     if(floor(playerStats.body.arms) != this.levelReached){
        //         this.levelReached = floor(playerStats.body.arms);
        //         this.playBarSfx(this.levelReached);
                
        //     }
        //     graphicsHandler.screenShake(5,.9);
        // }
        // this.sprite.depth = this.y -60;
    }

    draw(){
        // super.draw();
         if(this.active && !this.used){
            this.showButtons("c");
         }
        //     let barHeight = (playerStats.body.arms * this.barHeight) /3; //4 cuz 4 levels of arms
        //     fill(0,200,0);
        //     rect(this.barX,this.barY + this.barHeight - barHeight,this.barWidth,barHeight);
        // }
    }
}