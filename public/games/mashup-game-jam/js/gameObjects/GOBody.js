class GObody extends GOMachine{
    x = 400;
    y = 700;
    range = 64;
    levelReached = playerStats.body.body;
    balanced = 0;
    gravity = 1.1;
    speed = 0;
    acceleration = 6.4; 
    gravityMax = 6;

    init(){
        this.balanced = 0;
        this.speed = random(-2,2);
    }

    step(){
        super.step();
        if(this.active){
            if(keyWentDown(CONTROLS.LEFT)){
                this.speed += this.acceleration;
            }
            if(keyWentDown(CONTROLS.RIGHT)){
                this.speed -= this.acceleration;
            }

            if(keyWentDown(CONTROLS.LEFT) || keyWentDown(CONTROLS.RIGHT)){
                graphicsHandler.screenShake(5,.9);
                AudioHandler.playBody();
            }
            if(this.balanced >= -this.barHeight/10 && this.balanced <= this.barHeight/10){
                let modifier = 0.0005;
                playerStats.body.body += modifier * playerStats.stamina;
                playerStats.stamina = Utils.approach(playerStats.stamina, playerStats.staminaBase, .5);
                playerStats.body.body = constrain(playerStats.body.body, 0,3);
                if(floor(playerStats.body.body) != this.levelReached){
                    this.levelReached = floor(playerStats.body.body);
                    this.playBarSfx(this.levelReached);
                    graphicsHandler.screenShake(150,.9);
                }
            }
            this.speed *= this.gravity;
            this.speed = constrain(this.speed,-this.gravityMax,this.gravityMax);
            this.balanced -= this.speed;
            this.balanced = constrain(this.balanced,-this.barHeight/2,this.barHeight/2);
        }
        //this.sprite.depth = this.y -60;
    }

    draw(){
        if(this.active){
            let barHeight = (playerStats.body.body * this.barHeight) /3; //4 cuz 4 levels of arms
            fill(8,25,43);
            rect(this.barX,this.barY + this.barHeight - barHeight,this.barWidth,barHeight);
            stroke(255,255,0);
            strokeWeight(5);
            noFill();
            rect(this.barX,this.barY + this.barHeight/10*4,this.barWidth,this.barHeight/10*2);
            if(this.balanced >= -this.barHeight/10 && this.balanced <= this.barHeight/10){
                stroke(0,255,0);
            }else{
                stroke(255,0,0);
            }
            line(this.barX, this.barHeight/2+this.balanced,this.barX + this.barWidth, this.barHeight/2+this.balanced);
            noStroke();
            super.draw();
            this.showButtons("x");
            this.showButtons("z");
        }

    }
}