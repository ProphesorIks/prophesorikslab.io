class GOHead extends GOMachine{
    x = 72;
    y = 400;
    range = 64;
    levelReached = playerStats.body.head;
    
    letterTime = 300;
    timeModifier = .99;
    letterTimeOnScreen = 0; 

    letter = "c";

    init(){
        this.speed = 1;
        this.letterTime = 300;
        this.letterTimeOnScreen = this.letterTime;
        this.resetLetter();
    }

    step(){
        super.step();
            if(this.active){
            //graphicsHandler.screenShake(5,.9);
            if(keyWentDown(this.letter)){
                playerStats.body.head += .15 * ((this.letterTimeOnScreen * .01));
                this.resetLetter();
                AudioHandler.playHead();
            }else{
                if(keyWentDown("z") || keyWentDown("c") || keyWentDown("x")){
                    playerStats.body.head = floor(playerStats.body.head);
                    this.resetLetter();
                    AudioHandler.playHead();
                }
            }

            if(floor(playerStats.body.head) != this.levelReached){
                this.levelReached = floor(playerStats.body.head);
                this.playBarSfx(this.levelReached);
                graphicsHandler.screenShake(150,.9);
            }

            this.letterTimeOnScreen = Utils.approach(this.letterTimeOnScreen, 0, 5);
            if(this.letterTimeOnScreen <= 0){
                this.resetLetter();
            }            
        }
        this.sprite.depth = this.y -60;
        playerStats.body.head = constrain(playerStats.body.head, 0, 3);
    }

    draw(){
        
        if(this.active){
            let barHeight = (playerStats.body.head * this.barHeight) /3; //4 cuz 4 levels of arms
            fill(8,25,43);
            rect(this.barX,this.barY + this.barHeight - barHeight,this.barWidth,barHeight);
            fill(57,90,127);
            let progressHeight = (.2 * (this.letterTimeOnScreen * .01) * this.barHeight)/3;
            rect(this.barX,this.barY + this.barHeight - barHeight - progressHeight,this.barWidth,progressHeight);
            if(this.letter == "x"){
                image(graphicsHandler.assets.sprites.buttons.x.up,GraphicsHandler.WORLD_WIDTH/2,200);
            }
            if(this.letter == "z"){
                image(graphicsHandler.assets.sprites.buttons.z.up,GraphicsHandler.WORLD_WIDTH/2,200);
            }
            if(this.letter == "c"){
                image(graphicsHandler.assets.sprites.buttons.c.up,GraphicsHandler.WORLD_WIDTH/2,200);
            }
        }
        super.draw();
    }

    resetLetter(){
        this.letter = random(["x","z","c"]);      
        this.letterTime *= this.timeModifier;
        this.letterTime = max(this.letterTime,50);
        this.letterTimeOnScreen = this.letterTime;
    }
}