class GOLegs extends GOMachine{
    x = 72;
    y = 660;
    range = 64;
    levelReached = playerStats.body.legs;
    marker = 0;
    markerSpeed = 4;
    maxMarkerSpeed = 40;
    boxHeight = 0;
    boxY = 0;

    init(){
        this.markerSpeed = 4;
        this.resetMarker();
    }

    step(){
        super.step();
        if(this.active){
            if(keyWentDown(CONTROLS.USE)){
                if(this.marker >= this.boxY && this.marker <= this.boxY + this.boxHeight){
                    let modifier = 0.0;
                    let basePoints = .8;
                    basePoints += this.markerSpeed/4;
                    playerStats.body.legs += modifier * (playerStats.chalk + 1);
                    playerStats.body.legs += basePoints * 0.011;
                    playerStats.chalk = Utils.approach(playerStats.chalk, 0, .3);
                    playerStats.body.legs = constrain(playerStats.body.legs, 0,3);

                    if(floor(playerStats.body.legs) != this.levelReached){
                        this.levelReached = floor(playerStats.body.legs);
                        this.playBarSfx(this.levelReached);  
                        graphicsHandler.screenShake(150,.9);
                    }
                
                }else{
                    playerStats.body.legs -= .025;
                }
                playerStats.stamina = Utils.approach(playerStats.stamina, playerStats.staminaBase, .05);
                playerStats.body.legs = constrain(playerStats.body.legs, 0,3);
                this.resetMarker();
                AudioHandler.playLegs();
                graphicsHandler.screenShake(5,.9);
            }
            this.marker += this.markerSpeed * (deltaTime / 50);
            if(this.marker >= this.barHeight){
                this.resetMarker();
            }
        }
        // this.sprite.depth = this.y -60;
    }

    draw(){
         
        if(this.active){
           let barHeight = (playerStats.body.legs * this.barHeight) /3; //4 cuz 4 levels of arms
            fill(8,25,43);
            rect(this.barX,this.barY + this.barHeight - barHeight,this.barWidth,barHeight);
            if(this.marker >= this.boxY && this.marker <= this.boxY + this.boxHeight){
                stroke(0,255,0);
            }else{
                stroke(255,0,0);
            }
           
            strokeWeight(5);
            noFill();
            line(this.barX, this.marker,this.barX + this.barWidth, this.marker);
            stroke(0,255,0);
            rect(this.barX,this.boxY,this.barWidth,this.boxHeight);
            noStroke();
            this.showButtons("c");
        }
        super.draw();
    }

    resetMarker(){
        if(this.markerSpeed <= this.maxMarkerSpeed){
            this.markerSpeed+=1;
        }
        this.marker = 0;
        this.boxHeight = random(40, 180);
        this.boxY = random(0, this.barHeight - this.boxHeight);
    }
}