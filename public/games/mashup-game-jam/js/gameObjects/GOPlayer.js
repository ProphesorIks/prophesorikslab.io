class GOPlayer extends GameObject{
    movementSpeedBase = 10; //base speed
    movementSpeed = this.movementSpeedBase;
    acceleration = 2;
    friction = .5;
    speed = createVector(0, 0);
    isControlled = true;
    staminaMult = .95;
    hitbox = {
        width: 50,
        height: 180
    }

    static controlls = {
        UP: 'w',
        DOWN: 's',
        LEFT: 'a',
        RIGHT: 'd',
        SPRINT: 16
    };

    body = {
        legs: 0,
        arms: 1,
        body: 2,
        head: 3
    }

    bodyPrevArms = playerStats.body.arms;
    bodyPrevBody = playerStats.body.body;
    bodyPrevLegs = playerStats.body.legs;
    bodyPrevHead = playerStats.body.head;

    constructor(x, y) {
        super(x,y);
        this.sprite = {
            legs: graphicsHandler.assets.animations.player.b0.legs,
            body: graphicsHandler.assets.animations.player.b0.body,
            arms: graphicsHandler.assets.animations.player.b0.arms,
            head: graphicsHandler.assets.animations.player.b0.head
        };
        this.forBody((part) =>{
            part.visible = true;
        });
    }

    step(){
        this.movement();

        if(this.speed.x != 0 || this.speed.y != 0){
            AudioHandler.playFootSteps();
        }

        this.constrainToWorld();
        this.updateSprite();
        this.animate();

        if(this.bodyPrevArms != playerStats.body.arms){
            this.setBodyPartLevel(this.body.arms,playerStats.body.arms);
        }
        if(this.bodyPrevLegs != playerStats.body.legs){
            this.setBodyPartLevel(this.body.legs,playerStats.body.legs);
        }
        if(this.bodyPrevBody= playerStats.body.body){
            this.setBodyPartLevel(this.body.body,playerStats.body.body);
        }
        if(this.bodyPrevHead != playerStats.body.head){
            this.setBodyPartLevel(this.body.head,playerStats.body.head);
        }

        this.bodyPrevArms = playerStats.body.arms;
        this.bodyPrevBody = playerStats.body.body;
        this.bodyPrevLegs = playerStats.body.legs;
        this.bodyPrevHead = playerStats.body.head;
    };

    movement(){
        let inputHorizontal = 0;
        let inputVertical = 0;
        if(this.isControlled){
            inputHorizontal = keyDown(GOPlayer.controlls.RIGHT) - keyDown(GOPlayer.controlls.LEFT);
            inputVertical = keyDown(GOPlayer.controlls.DOWN) - keyDown(GOPlayer.controlls.UP);
        }

        this.speed.x += inputHorizontal * this.acceleration;
        this.speed.y += inputVertical * this.acceleration;

        this.speed.x = Utils.approach(this.speed.x, 0, this.friction);
        this.speed.y = Utils.approach(this.speed.y, 0, this.friction);

        this.speed.x = constrain(this.speed.x, -this.movementSpeed, this.movementSpeed);
        this.speed.y = constrain(this.speed.y, -this.movementSpeed, this.movementSpeed);

        this.movementSpeed = Utils.approach(this.movementSpeed, this.movementSpeedBase, this.movementSpeed/8);

        // if(keyDown(GOPlayer.controlls.SPRINT)){
        //     this.movementSpeed += playerStats.stamina * this.movementSpeedBase/2;
        //     playerStats.stamina *= this.staminaMult;
            
        // }
        this.x += this.speed.x * (deltaTime / 50);
        this.y += this.speed.y * (deltaTime / 50);
    }

    animate(){
        if(keyDown(GOPlayer.controlls.UP)){
            this.forBody((part) => {
                part.changeAnimation("up");
            });
        }

        if(keyDown(GOPlayer.controlls.DOWN)){
            this.forBody((part) => {
                part.changeAnimation("down");
            });
            
        }stop();

        if(keyDown(GOPlayer.controlls.RIGHT)){
            this.forBody((part) => {
                part.changeAnimation("right");
            });
            
        }

        if(keyDown(GOPlayer.controlls.LEFT)){
            this.forBody((part) => {
                part.changeAnimation("left");
            });
        }

        if(this.speed.x == 0 && this.speed.y == 0){
            this.forBody((part) => {
                part.animation.stop();
                part.animation.changeFrame(0);
            });
        }else{
            this.forBody((part) => {
                part.animation.play();
            });
        }

        //change depth
        //change which sprites are drawn based on what level the player is
        this.forBody((part) => {
            let d = this.y - this.hitbox.height/4;
            if(part == this.sprite.legs){
                part.depth = d -3; 
            }
            if(part ==this.sprite.body){
                part.depth = d - 1; 
            }
            if(part ==this.sprite.arms){
                part.depth = d- 2; 
            }
            if(part ==this.sprite.head){
                part.depth = d; 
            }

            //console.log("turbo-log: GOPlayer -> animate -> part.depth", part.depth);
        });



    }

    updateSprite(){
        this.forBody((bodyPart) => {
            bodyPart.position.x = this.x;
            bodyPart.position.y = this.y;
        });
    }

    setBodyPartLevel(bodyPart, level){
        level = constrain(level, 0, 3); //FIXME: move to variable
        level = floor(level);
        let bodySprites;

        if(level == 0){
            bodySprites = graphicsHandler.assets.animations.player.b0;
        }
        if(level == 1){
            bodySprites = graphicsHandler.assets.animations.player.b1;
        }
        if(level == 2){
            bodySprites = graphicsHandler.assets.animations.player.b2;
        }
        if(level == 3){
            bodySprites = graphicsHandler.assets.animations.player.b3;
        }

        if(bodyPart == this.body.legs){
            this.sprite.legs.visible = false;
            this.sprite.legs = bodySprites.legs;
            this.sprite.legs.visible = true;
        }
        if(bodyPart == this.body.body){
            this.sprite.body.visible = false;
            this.sprite.body = bodySprites.body;
            this.sprite.body.visible = true;
        }
        if(bodyPart == this.body.arms){
            this.sprite.arms.visible = false;
            this.sprite.arms = bodySprites.arms;
            this.sprite.arms.visible = true;
        }
        if(bodyPart == this.body.head){
            this.sprite.head.visible = false;
            this.sprite.head = bodySprites.head;
            this.sprite.head.visible = true;
        }
        
    }

    delete(){
        this.forBody(function(bodyPart){
                bodyPart.visible = false;
            }
        );
    }

    constrainToWorld(){
        this.x = constrain(this.x, this.hitbox.width / 2, GraphicsHandler.WORLD_WIDTH - this.hitbox.width/2)
        this.y = constrain(this.y, this.hitbox.height / 2, GraphicsHandler.WORLD_HEIGHT - this.hitbox.height/2);
    }

    forBody(func){
        Object.keys(this.sprite).forEach(function (bodyPart, index) {
            func(this.sprite[bodyPart]);
        }, this);
    }
}
