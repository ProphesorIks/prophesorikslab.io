class GOArms extends GOMachine{
    x = 433;
    y = 540;
    range = 64;
    levelReached = playerStats.body.arms;

    step(){
        super.step();
        if(this.active){
            if(keyWentDown(CONTROLS.LEFT) || keyWentDown(CONTROLS.RIGHT)){
                let modifier = 0.005;
                playerStats.body.arms += modifier * (playerStats.chalk + 1) * playerStats.stamina;
                playerStats.chalk = Utils.approach(playerStats.chalk, 0, .05);
                playerStats.stamina = Utils.approach(playerStats.stamina, playerStats.staminaBase, .05);
                playerStats.body.arms = constrain(playerStats.body.arms, 0,3);
                if(floor(playerStats.body.arms) != this.levelReached){
                    this.levelReached = floor(playerStats.body.arms);
                    this.playBarSfx(this.levelReached);
                    graphicsHandler.screenShake(150,.9);
                }
                graphicsHandler.screenShake(5,.9);
                AudioHandler.playArms();
            }
        }
        this.sprite.depth = this.y -60;
    }

    draw(){
        if(this.active){
            let barHeight = (playerStats.body.arms * this.barHeight) /3; //4 cuz 4 levels of arms
            fill(8,25,43);
            rect(this.barX,this.barY + this.barHeight - barHeight,this.barWidth,barHeight);
            super.draw();
            this.showButtons("x");
            this.showButtons("z");
        }
        
    }
}