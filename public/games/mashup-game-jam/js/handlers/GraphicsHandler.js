class GraphicsHandler { 
    static MISSING_TEXTURE_SIZE = 128;
    static WORLD_WIDTH = 480;
    static WORLD_HEIGHT = 1080;
    static VIEWPORT_WIDTH = 480;
    static VIEWPORT_HEIGHT = 720;
    sky;
    skyX = 0;
    skySpeed = 2;
    guiX = 0;
    guiY = 0;

    //Every game state has it's own sprite group to display only the needed sprites
    spriteGroups = {
        BACKGROUND: new Group(),
        OBJECTS: new Group(),
        GUI: new Group()
    };

    //camera properties
    camera = {
        xTo: camera.position.x,
        yTo: camera.position.y,
        ease: 25,
        shake: 0,
        shakeFalloff: .9
    };

    transitionCompletness = 0;
    transitionSpeed = 0;
    transitionCallback = () => {};
    transitionColor = 0;

    //Game assets structure
    assets = {
        background: createSprite(GraphicsHandler.WORLD_WIDTH/2,GraphicsHandler.WORLD_HEIGHT/2,GraphicsHandler.WORLD_WIDTH, GraphicsHandler.WORLD_HEIGHT),
        animations: {
            player: {
                b0: {
                    legs: createSprite(10,10,10,10),
                    body: createSprite(10,10,10,10),
                    arms: createSprite(10,10,10,10),
                    head: createSprite(10,10,10,10)
                },
                b1:{
                    legs: createSprite(10,10,10,10),
                    body: createSprite(10,10,10,10),
                    arms: createSprite(10,10,10,10),
                    head: createSprite(10,10,10,10)
                },
                b2:{
                    legs: createSprite(10,10,10,10),
                    body: createSprite(10,10,10,10),
                    arms: createSprite(10,10,10,10),
                    head: createSprite(10,10,10,10)
                },
                b3:{
                    legs: createSprite(10,10,10,10),
                    body: createSprite(10,10,10,10),
                    arms: createSprite(10,10,10,10),
                    head: createSprite(10,10,10,10)
                }
            }
        },
        sprites: {
            holy: createSprite(0,0,0,0),
            machines: {
                head: createSprite(0,0,0,0),
                arms: createSprite(0,0,0,0),
                body: createSprite(0,0,0,0),
                legs: createSprite(0,0,0,0),
            },
            chalk: createSprite(0,0,0,0),
            drugs: createSprite(0,0,0,0),
            treadmill: createSprite(0,0,0,0),
            holyDumbells: createSprite(0,0,0,0),
            bar: createSprite(0,0,0,0),
            buttons: {

                z:{
                    up:createSprite(0,0,0,0),
                    down: createSprite(0,0,0,0)
                } ,
                x:{
                    up:createSprite(0,0,0,0),
                    down: createSprite(0,0,0,0)
                },
                c:{
                    up:createSprite(0,0,0,0),
                    down: createSprite(0,0,0,0)
                } 
            }
        }
    };

    renderStack = [];
    cutsceneBack = createSprite(0,0,0,0);
    menuBack = createSprite(0,0,0,0);

    constructor() {

    }

    //Loads all graphics assets to memory where they are stored in assets variable
    loadAssets() {
        const FORMAT = ".png";
        const PATH = "assets/graphics/";
        const PATH_ANIMATIONS = "animations/";
        const PATH_BACKGROUNDS = "backgrounds/";
        const PATH_SPRITES = "sprites/";

        //load background
        this.cutsceneBack.addImage("cutscene-lower", loadImage(PATH + PATH_BACKGROUNDS + "cutscene-lower" + FORMAT));
        this.spriteGroups.BACKGROUND.add(this.cutsceneBack);
        this.cutsceneBack.position.y = 1080*2;
        this.cutsceneBack.position.x = 480/2;

        this.assets.background.addImage("gym", loadImage(PATH + PATH_BACKGROUNDS + "gym" + FORMAT));
        this.assets.background.addImage("cutscene", loadImage(PATH + PATH_BACKGROUNDS + "cutscene" + FORMAT));
        

        this.sky = loadImage(PATH + PATH_BACKGROUNDS + "sky" + FORMAT);
        this.menuBack = loadImage(PATH + PATH_BACKGROUNDS + "menu" + FORMAT);

        this.spriteGroups.BACKGROUND.add(this.assets.background);

        //load player animation
        let playerPath = PATH + PATH_ANIMATIONS + "player/";
        let dirs = ["up","down","left","right"];
        
        // load all the parts
        {
            let playerAnimations = this.assets.animations.player;
            Object.keys(playerAnimations).forEach((level,index) => {
                let bodyParts = playerAnimations[level];
                Object.keys(bodyParts).forEach(function (bodyPart,index) {
                    dirs.forEach(dir => {
                        bodyParts[bodyPart].addAnimation(
                            dir,
                            playerPath + level + "/" + bodyPart + "-" + dir + "-0" + FORMAT,
                            playerPath + level + "/" + bodyPart + "-" + dir + "-1" + FORMAT,
                            playerPath + level + "/" + bodyPart + "-" + dir + "-2" + FORMAT,
                        );
                    });
                    this.spriteGroups.OBJECTS.add(bodyParts[bodyPart]);
            
                    bodyParts[bodyPart].visible = false;
                }, this);
            });
        }
        //

        //{
        {
            let holy = this.assets.sprites.holy;
            holy.addImage("holy", loadImage(PATH + PATH_SPRITES + "holy-weights-cut" + FORMAT));
            this.spriteGroups.OBJECTS.add(holy);
            holy.visible = false;
        }

        //load machines
        {
            let arms = this.assets.sprites.machines.arms;
            arms.addImage("machines-arms", loadImage(PATH + PATH_SPRITES + "arms" + FORMAT));
            this.spriteGroups.OBJECTS.add(arms);
            arms.visible = false;
        }

        {
            let head = this.assets.sprites.machines.head;
            head.addImage("machines-head", loadImage(PATH + PATH_SPRITES + "head" + FORMAT));
            this.spriteGroups.OBJECTS.add(head);
            head.visible = false;
        }
        {
            let body = this.assets.sprites.machines.body;
            body.addImage("machines-body", loadImage(PATH + PATH_SPRITES + "body" + FORMAT));
            this.spriteGroups.OBJECTS.add(body);
            body.visible = false;
        }
        {
            let legs = this.assets.sprites.machines.legs;
            legs.addImage("machines-legs", loadImage(PATH + PATH_SPRITES + "legs" + FORMAT));
            this.spriteGroups.OBJECTS.add(legs);
            legs.visible = false;
        }
        {
            let chalk = this.assets.sprites.chalk;
            chalk.addImage("chalk", loadImage(PATH + PATH_SPRITES + "chalk" + FORMAT));
            this.spriteGroups.OBJECTS.add(chalk);
            chalk.visible = false;
        }
        {
            let drugs = this.assets.sprites.drugs;
            drugs.addImage("drugs", loadImage(PATH + PATH_SPRITES + "drugs" + FORMAT));
            this.spriteGroups.OBJECTS.add(drugs);
            drugs.visible = false;
        }
        {
            let treadmill = this.assets.sprites.treadmill;
            treadmill.addImage("treadmill", loadImage(PATH + PATH_SPRITES + "treadmill" + FORMAT));
            this.spriteGroups.OBJECTS.add(treadmill);
            treadmill.visible = false;
        }
        {
            let holyDumbells = this.assets.sprites.holyDumbells;
            holyDumbells.addImage("holyDumbells", loadImage(PATH + PATH_SPRITES + "holy-weights" + FORMAT));
            this.spriteGroups.OBJECTS.add(holyDumbells);
            holyDumbells.visible = false;
        }
        {
            this.assets.sprites.bar = loadImage(PATH + PATH_SPRITES + "bar" + FORMAT);
            {
                this.assets.sprites.buttons.z.up = loadImage(PATH + PATH_SPRITES + "z-up" + FORMAT);
                this.assets.sprites.buttons.z.down = loadImage(PATH + PATH_SPRITES + "z-down" + FORMAT);
                this.assets.sprites.buttons.c.up = loadImage(PATH + PATH_SPRITES + "c-up" + FORMAT);
                this.assets.sprites.buttons.c.down = loadImage(PATH + PATH_SPRITES + "c-down" + FORMAT);
                this.assets.sprites.buttons.x.up = loadImage(PATH + PATH_SPRITES + "x-up" + FORMAT);
                this.assets.sprites.buttons.x.down = loadImage(PATH + PATH_SPRITES + "x-down" + FORMAT);
            }
        }

    };

    //render gameObjects
    draw() {
        this.stepTransition();
        camera.off();
        background(50);
        camera.on();
        if(true){
            imageMode()
            image(this.sky, this.skyX, this.sky.height/2, this.sky.width, this.sky.height);
            image(this.sky, this.skyX - this.sky.width, this.sky.height/2, this.sky.width, this.sky.height);
            this.skyX += this.skySpeed * (deltaTime / 50);
            this.skyX %= this.sky.width;
        }

        drawSprites(this.spriteGroups.BACKGROUND);
        drawSprites(this.spriteGroups.OBJECTS);
        drawSprites(this.spriteGroups.GUI);
        if(GAME_STATES.CUTSCENE == gameState){
            drawSprites(this.spriteGroups.OBJECTS_CUTSCENE);
            camera.off();

            //this.drawStaminaBar();
            camera.on();
        }
        if(GAME_STATES.MENU == gameState){
            camera.off();
            image(this.menuBack,width/2,height/2,width,height);
            camera.on();
        }
        if(GAME_STATES.GYM == gameState){
            camera.off();
            if(room.gym.machineArms != null){
                room.gym.machineArms.draw();
                //room.gym.machineArms.sprite.groups
                //console.log("turbo-log: GraphicsHandler -> draw -> room.gym.machineArms.sprite.groups", room.gym.machineArms.sprite);
            }
            if(room.gym.machineHead != null){
                room.gym.machineHead.draw();
                //room.gym.machineArms.sprite.groups
                //console.log("turbo-log: GraphicsHandler -> draw -> room.gym.machineArms.sprite.groups", room.gym.machineArms.sprite);
            }
            if(room.gym.machineLegs != null){
                room.gym.machineLegs.draw();
                //room.gym.machineArms.sprite.groups
                //console.log("turbo-log: GraphicsHandler -> draw -> room.gym.machineArms.sprite.groups", room.gym.machineArms.sprite);
            }
            if(room.gym.machineBody != null){
                room.gym.machineBody.draw();
                //room.gym.machineArms.sprite.groups
                //console.log("turbo-log: GraphicsHandler -> draw -> room.gym.machineArms.sprite.groups", room.gym.machineArms.sprite);
            }
            if(room.gym.chalk != null){
                room.gym.chalk.draw();
                //room.gym.machineArms.sprite.groups
                //console.log("turbo-log: GraphicsHandler -> draw -> room.gym.machineArms.sprite.groups", room.gym.machineArms.sprite);
            }
            if(room.gym.drugs != null){
                room.gym.drugs.draw();
                //room.gym.machineArms.sprite.groups
                //console.log("turbo-log: GraphicsHandler -> draw -> room.gym.machineArms.sprite.groups", room.gym.machineArms.sprite);
            }
            if(room.gym.treadmill != null){
                room.gym.treadmill.draw();
                //room.gym.machineArms.sprite.groups
                //console.log("turbo-log: GraphicsHandler -> draw -> room.gym.machineArms.sprite.groups", room.gym.machineArms.sprite);
            }
            if(room.gym.holyDumbells != null){
                room.gym.holyDumbells.draw();
                //room.gym.machineArms.sprite.groups
                //console.log("turbo-log: GraphicsHandler -> draw -> room.gym.machineArms.sprite.groups", room.gym.machineArms.sprite);
            }
            camera.on();
            camera.off();
            this.drawStaminaBar();
            this.drawChalkBar();
            camera.on();
        }
        if(GAME_STATES.LIFT == gameState){
            camera.off();
            this.drawStaminaBar();
            this.drawChalkBar();
            camera.on();
        }
        camera.off();
        this.drawGui();
        camera.on();
    };

    drawObjects(){
        drawSprites(this.spriteGroups.OBJECTS);
    }

    drawGui(){
        //push();
        //screen shake
        this.guiX += random(-this.camera.shake, this.camera.shake);
        this.guiY += random(-this.camera.shake, this.camera.shake);

        drawSprites(this.spriteGroups.GUI);

        if(this.transitionCompletness != 0){
            let a = this.transitionCompletness;
            if(this.transitionSpeed < 0){
                a = 1 - a;
            }
            fill("rgba("+this.transitionColor +","+this.transitionColor +","+this.transitionColor +", " + a + ")");
            rect(0,0,width,height);
        }

        this.guiX = ((0 - this.guiX) / this.camera.ease) * (deltaTime / 50);
        this.guiY = ((0 - this.guiY) / this.camera.ease) * (deltaTime / 50);
        //translate(0,0);
        //pop();
    }

    //sets where to point the camera
    pointCamera(x, y) {
        this.camera.xTo = x;
        this.camera.yTo = y;
    };

    changeWorldSize(w, h){
        GraphicsHandler.WORLD_WIDTH = w;
        GraphicsHandler.WORLD_HEIGHT = h;
    }

    //eases the camera and updates in game camera to follow the virtual camera
    updateCamera() {
        camera.position.x += ((this.camera.xTo - camera.position.x) / this.camera.ease) * (deltaTime / 50);
        camera.position.y += ((this.camera.yTo - camera.position.y) / this.camera.ease) * (deltaTime / 50);

        //don't let camera go outside of the world
        camera.position.x = constrain(
            camera.position.x,
            GraphicsHandler.VIEWPORT_WIDTH / 2,
            GraphicsHandler.WORLD_WIDTH - GraphicsHandler.VIEWPORT_WIDTH / 2
        );
        camera.position.y = constrain(
            camera.position.y,
            GraphicsHandler.VIEWPORT_HEIGHT / 2,
            GraphicsHandler.WORLD_HEIGHT - GraphicsHandler.VIEWPORT_HEIGHT / 2
        );

        //screen shake
        camera.position.x += random(-this.camera.shake, this.camera.shake);
        camera.position.y += random(-this.camera.shake, this.camera.shake);
        this.camera.shake *= this.camera.shakeFalloff;
    };
    //shakes camera. fallof < 1 - shake wears down
    screenShake(strength, falloff) {
        this.camera.shake = max(this.camera.shake, strength);
        this.camera.shakeFalloff = falloff;
    };

    static resizeAnimation(animation, width, heigth){
        animation.forEach(element => {
            element.width = width;
            element.height = heigth;
        });
    }

    //Generates a checkerboard pattern for a missing texture texture.
    generateMissingTexture() {
        let img = createImage(MISSING_TEXTURE_SIZE, MISSING_TEXTURE_SIZE);
        img.loadPixels();
        for (let i = 0; i < img.width; i++) {
            for (let j = 0; j < img.height; j++) {
                img.set(i, j, color((i % 32 > 16 ? 0 : 1) * 255, (j % 32 > 16 ? 0 : 1) * 255, (i % 32 < 16 ? 0 : 1) * 255));
            }
        }
        img.updatePixels();
        return img;
    };

    drawStaminaBar(){
        let height = 20;
        let width = (playerStats.stamina * GraphicsHandler.VIEWPORT_WIDTH) / 3;
        fill("rgba(255,0,0,.5)");
        rect(0, GraphicsHandler.VIEWPORT_HEIGHT - height, width, height);
    }

    drawChalkBar(){
        let height = 7;
        let width = (playerStats.chalk * GraphicsHandler.VIEWPORT_WIDTH) / 3;
        fill("rgba(255,255,255,.5)");
        rect(0, GraphicsHandler.VIEWPORT_HEIGHT - height - 20, width, height); //FIXME: magic numver
    }

    transition(speed,color,callBack){
        this.transitionCompletness = 1;
        this.transitionSpeed = speed;
        this.transitionColor = color;
        this.transitionCallback = callBack;
    };

    stepTransition(){
        if(this.transitionCompletness > 0){
            this.transitionCompletness = Utils.approach(this.transitionCompletness, 0, abs(this.transitionSpeed));
            if(this.transitionCompletness <= 0){
                this.transitionCallback();
            }
        }
    }
}

