class AudioHandler{
    static sfx = new Map();
    static music = new Map();
    static categories = [
        "bar",
        "bench-press",
        "core-press",
        "footsteps",
        "god",
        "head-press",
        "holy-grail",
        "leg-press",
        "treadmill"
    ]

    constructor(){
        userStartAudio();
    }

    loadAssets(){
        const FORMAT = ".ogg";
        const PATH = "assets/audio/";
        const PATH_SFX = PATH + "sounds/";
        const PATH_MUSIC = PATH + "music/";

        soundFormats('mp3', 'ogg');
        
        // AudioHandler.addSfx("leather", loadSound(PATH_SFX + "leather" + FORMAT));
        // AudioHandler.addSfx("metal", loadSound(PATH_SFX + "metal" + FORMAT));

        //MUSIC
        AudioHandler.addMusic("game-loop", loadSound(PATH_MUSIC + "gameloop" + FORMAT));
        AudioHandler.addMusic("menu", loadSound(PATH_MUSIC + "menu" + FORMAT));
        AudioHandler.addMusic("win", loadSound(PATH_MUSIC + "win" + FORMAT));
        AudioHandler.addMusic("lose", loadSound(PATH_MUSIC + "lose" + FORMAT));
        AudioHandler.addMusic("lift", loadSound(PATH_MUSIC + "lift" + FORMAT));
        AudioHandler.addMusic("wind", loadSound(PATH_MUSIC + "wind" + FORMAT));

        //SFX
        //machine sfx
        {
            let machines = [
                "arms",
                "body",
                "head",
                "holy-grail",
                "legs",
                "treadmill"
            ];
            machines.forEach(machine => {
                for(let i = 0; i < 4; i++){
                    let sound = loadSound(PATH_SFX + machine + "/" + i + FORMAT, function(){
                        if(sound != undefined){
                            AudioHandler.addSfx(machine + "-" + i, sound);
                        }
                    }, function(){console.error("sound not found" + machine + "-" + i)});
                }
            });
        }

        //footsteps;
        {
            for(let i = 0; i < 4; i++){
                let prefix = "footsteps";
                let sound = loadSound(PATH_SFX + prefix + "/" + i + FORMAT, function(){
                    if(sound != undefined){
                        AudioHandler.addSfx(prefix + "-" + i, sound);
                    }
                }, function(){console.error("sound not found" + prefix + "-" + i)});
            }
        }
        //Bar
        {
            for(let i = 0; i < 3; i++){
                let prefix = "bar";
                let sound = loadSound(PATH_SFX + prefix + "/" + i + FORMAT, function(){
                    if(sound != undefined){
                        AudioHandler.addSfx(prefix + "-" + i, sound);
                    }
                }, function(){console.error("sound not found" + prefix + "-" + i)});
            }
        }

        //god
        {
            let prefix = "god";
            for(let i = 0; i < 3; i++){
                let sound = loadSound(PATH_SFX + prefix + "/" + i + FORMAT, function(){
                    if(sound != undefined){
                        AudioHandler.addSfx(prefix + "-" + i, sound);
                    }
                }, function(){console.error("sound not found" + prefix + "-" + i)});
            }
            let sound = loadSound(PATH_SFX + prefix + "/holy-weight" + FORMAT, function(){
                if(sound != undefined){
                    AudioHandler.addSfx(prefix + "-holy-weight", sound);
                }
            }, function(){console.error("sound not found" + prefix + "-holy-weight")});

            {
                let sound = loadSound(PATH_SFX + prefix + "/tutorial" + FORMAT, function(){
                    if(sound != undefined){
                        AudioHandler.addSfx(prefix + "-tutorial", sound);
                    }
                }, function(){console.error("sound not found" + prefix + "-tutorial")});
            }
            {
                let sound = loadSound(PATH_SFX + prefix + "/not-long-left" + FORMAT, function(){
                    if(sound != undefined){
                        AudioHandler.addSfx(prefix + "-not-long-left", sound);
                    }
                }, function(){console.error("sound not found" + prefix + "-not-long-left")});
            }
        }

        let ldSnd = function(name){
            let sound = loadSound(PATH_SFX +name + FORMAT, function(){
                if(sound != undefined){
                    AudioHandler.addSfx(name, sound);
                }
            }, function(){console.error("sound not found" + prefix + name)});
        }
        {
            ldSnd("chalk");
            ldSnd("end");
            ldSnd("lightning");
            ldSnd("drugs");

        }
        // sfx.forEach((element) => {
        //     if(element != undefined){
        //         element.setVolume(1);
        //     }
        // });
        // music.forEach((element) => {
        //     if(element != undefined){
        //         element.setVolume(1);
        //     }
        // });
    }

    static pauseSfx(){
        this.sfx.forEach(element => {
            if(element != undefined){
                element.pause();
            }
        });
    }

    static unpauseSfx(){
        this.sfx.forEach(element => {
            if(element != undefined){
                element.play();
            }
        });
    }

    static stopSfx(){
        this.sfx.forEach(element => {
            if(element != undefined){
                element.stop();
            }
        });
    }

    static pauseMusic(){
        this.music.forEach(element => {
            if(element != undefined){
                element.pause();
            }
        });
    }

    static unpauseMusic(){
        this.music.forEach(element => {
            if(element != undefined){
                element.pladb937e52c59d40518131a21959136b4373dd0e5ay();
            }
        });
    }

    static stopMusic(){
        this.music.forEach(element => {
            if(element != undefined){
                element.stop();
            }
        });
    }


    static addSfx(tag, sound){
        AudioHandler.sfx.set(tag, sound);
    }

    static getSfx(tag){
        return AudioHandler.sfx.get(tag);
    }

    static deleteSfx(tag){
        return AudioHandler.sfx.delete(tag);
    }

    static addMusic(tag, sound){
        AudioHandler.music.set(tag, sound);
    }

    static getMusic(tag){
        return AudioHandler.music.get(tag);
    }

    static deleteMusic(tag){
        return AudioHandler.music.delete(tag);
    }

    //others
    static playFootSteps(){
        let isSfxPlaying = false;
        let tag = "footsteps";
        let amount = 4;
        for(let i = 0; i < amount; i++){
            if(this.getSfx(tag + "-" +i).isPlaying()){
                isSfxPlaying = true;
                break;
            }
        }

        if(!isSfxPlaying){
            this.getSfx(tag + "-" + parseInt(random(0, amount))).play(.1);
        }
    }

    static playArms(){
        let isSfxPlaying = false;
        let tag = "arms";
        let amount = 4;
        for(let i = 0; i < amount; i++){
            if(this.getSfx(tag + "-" +i).isPlaying()){
                isSfxPlaying = true;
                break;
            }
        }

        if(!isSfxPlaying){
            this.getSfx(tag + "-" + parseInt(random(0, amount))).play();   
        }
    }

    static playBody(){
        let isSfxPlaying = false;
        let tag = "body";
        let amount = 4;
        for(let i = 0; i < amount; i++){
            if(this.getSfx(tag + "-" +i).isPlaying()){
                isSfxPlaying = true;
                break;
            }
        }

        if(!isSfxPlaying){
            this.getSfx(tag + "-" + parseInt(random(0, amount))).play();   
        }
    }

    static playLegs(){
        let isSfxPlaying = false;
        let tag = "legs";
        let amount = 4;
        for(let i = 0; i < amount; i++){
            if(this.getSfx(tag + "-" +i).isPlaying()){
                isSfxPlaying = true;
                break;
            }
        }

        //if(!isSfxPlaying){
            this.getSfx(tag + "-" + parseInt(random(0, amount))).play();   
        //}
    }

    static playTredmill(){
        let isSfxPlaying = false;
        let tag = "treadmill";
        let amount = 4;
        for(let i = 0; i < amount; i++){
            if(this.getSfx(tag + "-" +i).isPlaying()){
                isSfxPlaying = true;
                break;
            }
        }

        if(!isSfxPlaying){
            this.getSfx(tag + "-" + parseInt(random(0, amount))).play();   
        }
    }

    static playHolyGrail(){
        let isSfxPlaying = false;
        let tag = "holy-grail";
        let amount = 4;
        for(let i = 0; i < amount; i++){
            if(this.getSfx(tag + "-" +i).isPlaying()){
                isSfxPlaying = true;
                break;
            }
        }

        if(!isSfxPlaying){
            this.getSfx(tag + "-" + parseInt(random(0, amount))).play();   
        }
    }

    static playHead(){
        let isSfxPlaying = false;
        let tag = "head";
        let amount = 4;
        for(let i = 0; i < amount; i++){
            if(this.getSfx(tag + "-" +i).isPlaying()){
                isSfxPlaying = true;
                break;
            }
        }

        if(!isSfxPlaying){
            this.getSfx(tag + "-" + parseInt(random(0, amount))).play();   
        }
    }

    static playHoly(){
        let isSfxPlaying = false;
        let tag = "holy-grail";
        let amount = 4;
        for(let i = 0; i < amount; i++){
            if(this.getSfx(tag + "-" +i).isPlaying()){
                isSfxPlaying = true;
                break;
            }
        }

        if(!isSfxPlaying){
            this.getSfx(tag + "-" + parseInt(random(0, amount))).play();   
        }
    }
}