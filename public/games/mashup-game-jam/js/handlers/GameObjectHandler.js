class GameObjectHandler {
    objectStack = new Map();

    constructor() {

    }

    step() {
        this.objectStack.forEach(item => {
            if(item != undefined){
                item.step()
            }else{
                console.log("turbo-log: GameObjectHandler -> step -> "," Tried to call step for undefined GO");
            }
        });
    }

    addObject(tag, gameObject) {
        this.objectStack.set(tag, gameObject);
    }

    removeObject(tag) {
        this.objectStack.get(tag).delete();
        if(!this.objectStack.delete(tag)){
            console.log("tried to remove non present GO: " + tag);
        }
    }

    clear() {
        this.objectStack.forEach(item => item.delete());
        this.objectStack.clear();
    }

}
