//GAME STATES to manage how the game works
const GAME_STATES = {
  "MENU": 0,
  "GYM": 1,
  "LIFT": 2,
  "CUTSCENE": 3
}

const CONTROLS = {
  LEFT: 'z',
  RIGHT: 'x',
  USE: 'c',
  LIFT: 'l'
}

var gameState;
var cutsceneComplete = false;
let previousGameState;

var graphicsHandler;
var gameObjectHandler;
var audioHandler;

var playerStats = {};

var room;

function initHandlers(){
  graphicsHandler = new GraphicsHandler();
  audioHandler = new AudioHandler();
}

function preload() {
  previousGameState = GAME_STATES.CUTSCENE;
  initHandlers();
  graphicsHandler.loadAssets();
  audioHandler.loadAssets();
}

//set up  the 
function setup() {
  createCanvas(480, 720);
  frameRate(60);
  noCursor();
  strokeWeight(0);
  imageMode(CENTER);

  resetData();

  //Init other handlers
  gameObjectHandler = new GameObjectHandler();

  if(!cutsceneComplete){
    gameState = GAME_STATES.CUTSCENE;
    //startRoomCutscene();
  }else{
    gameState = GAME_STATES.MENU;
    //startRoomMenu();
  }

  AudioHandler.getMusic("wind").play();
  AudioHandler.getMusic("wind").setVolume(0,6);
  resetRoom();
  
}

function draw() {
  clear();
  if(gameState != GAME_STATES.PAUSE){ //TODO: reavaluate
    graphicsHandler.draw();
    step();
  }
}

function step(){
  graphicsHandler.updateCamera();
  gameObjectHandler.step();

  //If game state changed
  if(previousGameState != gameState){
    resetRoom();
  }

  if(GAME_STATES.CUTSCENE == gameState){
    stepRoomCutscene();
  }
  if(GAME_STATES.MENU == gameState){
    stepRoomMenu();
  }
  if(GAME_STATES.GYM == gameState){
    stepRoomGym();
  }
  if(GAME_STATES.LIFT == gameState){
    stepRoomLift();
  }

  previousGameState = gameState;
}

function resetRoom(){
  gameObjectHandler.clear();
  AudioHandler.stopSfx();
  AudioHandler.stopMusic();
  if(GAME_STATES.CUTSCENE == gameState){
    startRoomCutscene();
  }
  if(GAME_STATES.MENU == gameState){
    startRoomMenu();
  }
  if(GAME_STATES.GYM == gameState){
    startRoomGym();
  }
  if(GAME_STATES.LIFT == gameState){
    startRoomLift();
  }
}

function resetData(){
  room = {
    cutscene: {
      player: null,
      isTutorialTrigerred: false,
      isGodNarrating: false,
      isToLift: false
    },
    gym: {
      player: null,
      machineLegs: null,
      machineArms: null,
      machineHead: null,
      machineBody: null,
      chalk: null,
      drugs: null,
      treadmill: null,
      holyDumbells: null,
      timeOver: false
    },
    menu: {
      isLoading: false
    }
  }
  
  playerStats = {
    stamina: 1,
    staminaBaseLast: 1,
    staminaBase: 1,
    chalk: 0,
    body: {
      head: 0,
      legs: 0,
      body: 0,
      arms: 0,
    }
  }
}

function startRoomCutscene(){
  AudioHandler.getMusic("wind").play();
  AudioHandler.getMusic("wind").setVolume(.2,2);
  graphicsHandler.assets.background.visible = true;
  graphicsHandler.assets.background.changeImage("cutscene");
  graphicsHandler.transition(.01, 0, () => {console.log("Cutscene transition complete")});
  GraphicsHandler.WORLD_WIDTH = 480;
  GraphicsHandler.WORLD_HEIGHT = 3240;
  room.cutscene.player = new GOPlayer(480/2,2776);
  gameObjectHandler.addObject("player",room.cutscene.player);
  graphicsHandler.camera.ease = 25;
  playerStats.stamina = 3;
}

function timeEnded(){
  if(gameState != GAME_STATES.GYM){
    return;
  }
  
  AudioHandler.getMusic("lift").play();
  room.gym.timeOver = true;
  playerStats.staminaBaseLast = playerStats.staminaBase;
  AudioHandler.getMusic("game-loop").setVolume(0,3);
}

function startRoomGym(){
  AudioHandler.getMusic("wind").play();
  AudioHandler.getMusic("wind").setVolume(.05,6);
  graphicsHandler.assets.background.visible = true;
  let mus = AudioHandler.getMusic("game-loop");
  mus.setVolume(1);
  setTimeout(
    function(){
        timeEnded();
    }, 180000
  );
  //play one of gods voices
  setTimeout(
    function(){
      let rng = parseInt(random(0,2));
      let audio = AudioHandler.getSfx("god-" + rng);
      if(audio != undefined){
        if(gameState == GAME_STATES.GYM){
          audio.play();
        }
      }
    }, random(1000*60,1000*120)
  );

    //play not long left
    setTimeout(
      function(){
        let audio = AudioHandler.getSfx("god-not-long-left");
        if(audio != undefined){
          if(gameState == GAME_STATES.GYM){
            audio.play();
          }
        }
      }, 150000
    );

  if(!mus.isPlaying()){
    AudioHandler.getMusic("game-loop").play();
  }
  //create new game object and add it to gameHandler
  graphicsHandler.assets.background.changeImage("gym");
  GraphicsHandler.WORLD_WIDTH = 480;
  GraphicsHandler.WORLD_HEIGHT = 1080;
  room.gym.player = new GOPlayer(480/2, 720 - 200);
  gameObjectHandler.addObject("PLAYER",room.gym.player);
  room.gym.machineArms = new GOArms(0,0,graphicsHandler.assets.sprites.machines.arms);
  room.gym.machineLegs = new GOLegs(0,0,graphicsHandler.assets.sprites.machines.legs);
  room.gym.machineBody = new GObody(0,0,graphicsHandler.assets.sprites.machines.body);
  room.gym.machineHead = new GOHead(0,0,graphicsHandler.assets.sprites.machines.head);
  room.gym.chalk = new GOChalk(0,0,graphicsHandler.assets.sprites.chalk);
  room.gym.drugs = new GODrugs(0,0,graphicsHandler.assets.sprites.drugs);
  room.gym.treadmill = new GOTreadmill(0,0,graphicsHandler.assets.sprites.treadmill);
  room.gym.holyDumbells = new GODumbell(0,0,graphicsHandler.assets.sprites.holyDumbells);

  gameObjectHandler.addObject("arms", room.gym.machineArms);
  gameObjectHandler.addObject("legs", room.gym.machineLegs);
  gameObjectHandler.addObject("head", room.gym.machineHead);
  gameObjectHandler.addObject("body", room.gym.machineBody);
  gameObjectHandler.addObject("chalk", room.gym.chalk);
  gameObjectHandler.addObject("drugs", room.gym.drugs);
  gameObjectHandler.addObject("treadmill", room.gym.treadmill);
  gameObjectHandler.addObject("dumbells", room.gym.holyDumbells);
}

function startRoomMenu(){
  AudioHandler.getMusic("wind").setVolume(0,3);
  resetData();
  graphicsHandler.assets.background.visible = false;
  GraphicsHandler.WORLD_WIDTH = 480;
  GraphicsHandler.WORLD_HEIGHT = 720;
  graphicsHandler.assets.background.changeImage("menu");
  AudioHandler.getMusic("menu").play();
  AudioHandler.getMusic("menu").setVolume(1);
  AudioHandler.getMusic("menu").setLoop(true);
}

function startRoomLift(){
  AudioHandler.getMusic("wind").play();
  AudioHandler.getMusic("wind").setVolume(.2,6);
  graphicsHandler.assets.background.visible = false;
  GraphicsHandler.WORLD_WIDTH = 480;
  GraphicsHandler.WORLD_HEIGHT = 720;
  graphicsHandler.assets.background.changeImage("menu");
  gameObjectHandler.addObject("holy", new GOHoly(GraphicsHandler.WORLD_WIDTH/2,GraphicsHandler.WORLD_HEIGHT,graphicsHandler.assets.sprites.holy));
}

function stepRoomCutscene(){
  if(!room.cutscene.isGodNarrating){
    graphicsHandler.camera.xTo = room.cutscene.player.x;
    graphicsHandler.camera.yTo = room.cutscene.player.y;
  }else{
    graphicsHandler.camera.xTo = GraphicsHandler.WORLD_WIDTH/2;
    graphicsHandler.camera.yTo = 0;
  }
  if(playerStats.stamina < playerStats.staminaBase){
    playerStats.stamina = Utils.approach(playerStats.stamina, playerStats.staminaBase, .01);
  }

  //trigger tutorial
  if(!room.cutscene.isTutorialTrigerred && room.cutscene.player.y < 1080){
    AudioHandler.getSfx("god-tutorial").play();
    room.cutscene.isGodNarrating = true;
    graphicsHandler.camera.ease = 50;
    room.cutscene.player.isControlled = false;
    setTimeout(function(){
      graphicsHandler.screenShake(15,1);
    },400);
    //Delay shake a bit
    //when god is done talking reset to defaults
    setTimeout(function(){
      room.cutscene.isGodNarrating = false;
       graphicsHandler.camera.ease = 25;
       room.cutscene.player.isControlled = true;
       graphicsHandler.screenShake(15,.95);
    }, 16000);
    room.cutscene.isTutorialTrigerred = true;
  }

  //FIXME: replace value
  if(room.cutscene.player.y < 100 && room.cutscene.isToLift == false){
    AudioHandler.getSfx("lightning").play();
    graphicsHandler.screenShake(30,.9);
    graphicsHandler.transition(.02, 255, () => {
      graphicsHandler.transition(-.01, 0,() => {gameState = GAME_STATES.LIFT})
    });
    room.cutscene.isToLift = true;
    room.cutscene.player.isControlled = false;

  }
  //console.log("turbo-log: stepRoomCutscene -> playerStats.stamina", playerStats.stamina);
}

function stepRoomGym(){
  graphicsHandler.camera.xTo = room.gym.player.x;
  graphicsHandler.camera.yTo = room.gym.player.y;
  // if(playerStats.staminaBase < 2){
  //   playerStats.stamina = max(playerStats.stamina, playerStats.staminaBase);
  // }
  playerStats.chalk = constrain(playerStats.chalk, .2, 3);

  if(room.gym.timeOver){
    if(room.gym.player != null){
      if(room.gym.player.y > 200){
        room.gym.player.y = Utils.approach(room.gym.player.y,200,4);
        room.gym.player.x = Utils.approach(room.gym.player.x,480/2,2);
      }
    }
  }
}

function stepRoomMenu(){
  camera.off();
  Utils.showButtons('c');
  if(keyWentDown('c')){
    if(!room.menu.isLoading){
      graphicsHandler.transition(-.02,0,() => {gameState =GAME_STATES.GYM; AudioHandler.getMusic("menu").setVolume(0,2);});
      room.menu.isLoading = false;
    }
  }
  camera.on();
}

function stepRoomLift(){

}